ARG BUILDER_BASE_IMAGE=jupyter/scipy-notebook:python-3.9
FROM $BUILDER_BASE_IMAGE as builder

# ================================================================================
# Linux applications and libraries
# ================================================================================

# USER root
#
# RUN apt-get update \
#  && apt-get install --yes --quiet --no-install-recommends \
#     graphviz \
#  && apt-get clean \
#  && rm -rf /var/lib/apt/lists/*

# ================================================================================
# Install all needed Python Packages
# ================================================================================

USER ${NB_UID}

# Install from the requirements.txt file
COPY --chown=${NB_UID}:${NB_GID} requirements.in /tmp/
RUN pip install --no-cache-dir --requirement /tmp/requirements.in \
 && fix-permissions "${CONDA_DIR}" \
 && fix-permissions "/home/${NB_USER}"

# ================================================================================
# Setup the user
# ================================================================================

USER ${NB_UID}
WORKDIR "${HOME}"

COPY --chown=${NB_UID}:${NB_GID} assets/ assets/
COPY --chown=${NB_UID}:${NB_GID} data/ data/
COPY --chown=${NB_UID}:${NB_GID} kpca.ipynb .

