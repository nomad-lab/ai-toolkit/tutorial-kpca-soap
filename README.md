# tutorial-kpcs-soap



## Development

Cloning the source code:
```
git clone git@gitlab.mpcdf.mpg.de:nomad-lab/ai-toolkit/tutorial-kpca-soap.git
cd tutorial-kpca-soap
```

Running notebook image and mounting local folder into teh work directory:
```
docker run --rm -it -e DOCKER_STACKS_JUPYTER_CMD=notebook -p 8888:8888 -v $PWD:/home/jovyan/work gitlab-registry.mpcdf.mpg.de/nomad-lab/ai-toolkit/tutorial-kpca-soap:main
```

Building the image (advanced):
```
docker build --pull --rm -f "Dockerfile" -t gitlab-registry.mpcdf.mpg.de/nomad-lab/ai-toolkit/tutorial-kpca-soap:main "."
```

Running container with sudo feature (advanced):
```
docker run --rm -it --user root -e GRANT_SUDO=yes -e DOCKER_STACKS_JUPYTER_CMD=notebook -p 8888:8888 -v $PWD:/home/jovyan/work gitlab-registry.mpcdf.mpg.de/nomad-lab/ai-toolkit/tutorial-kpca-soap:main
```
